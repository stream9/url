#ifndef URI_DETAIL_VALIDATOR_HPP
#define URI_DETAIL_VALIDATOR_HPP

#include <url/validator.hpp>

#include <functional>
#include <string_view>

namespace urls::validator {

using iterator = std::string_view::iterator;
using error_handler_t = std::function<bool(iterator&, validation_errc)>;

inline bool
null_error_handler(iterator&, validation_errc)
{
    return true; // stop validation
}

/*
 * classifier
 */
bool is_alpha(char c);
bool is_digit(char c);
bool is_hexdig(char c);
bool is_unreserved(char c);
bool is_sub_delims(char c);

/*
 * non-terminal
 */
bool unreserved(iterator& begin, iterator end, error_handler_t);
bool hexdig(iterator& begin, iterator end, error_handler_t);
bool sub_delims(iterator& begin, iterator end, error_handler_t);
bool pct_encoded(iterator& begin, iterator end, error_handler_t);
bool dec_octet(iterator& begin, iterator end, error_handler_t);
bool pchar(iterator& begin, iterator end, error_handler_t);
bool segment(iterator& begin, iterator end, error_handler_t);
bool segment_nz(iterator& begin, iterator end, error_handler_t);
bool ipv4_address(iterator& begin, iterator end, error_handler_t);
bool reg_name(iterator& begin, iterator end, error_handler_t);
bool ipvfuture(iterator& begin, iterator end, error_handler_t);
bool h16(iterator& begin, iterator end, error_handler_t);
bool ls32(iterator& begin, iterator end, error_handler_t);
bool ipv6_address(iterator& begin, iterator end, error_handler_t);
bool ip_literal(iterator& begin, iterator end, error_handler_t);

/*
 * URI component
 */
bool scheme(std::string_view, error_handler_t = null_error_handler);
bool user_info(std::string_view, error_handler_t = null_error_handler);
bool host(std::string_view, error_handler_t = null_error_handler);
bool port(std::string_view, error_handler_t = null_error_handler);
bool path_abempty(std::string_view, error_handler_t = null_error_handler);
bool path_absolute(std::string_view, error_handler_t = null_error_handler);
bool path_rootless(std::string_view, error_handler_t = null_error_handler);
bool path_empty(std::string_view, error_handler_t = null_error_handler);
bool query(std::string_view, error_handler_t = null_error_handler);
bool fragment(std::string_view, error_handler_t = null_error_handler);

} // namespace urls::validator

#endif // URI_DETAIL_VALIDATOR_HPP
