#include <url/components.hpp>
#include <url/parser.hpp>
#include <url/url.hpp>

#include <ostream>
#include <string>
#include <string_view>

namespace urls {

template<typename Fn>
std::string_view
parse_component(std::string_view view, Fn& h)
{
    parse(view, h);

    return h.result;
}

// url

url::
url(std::string&& str)
    : m_str { std::move(str) }
{
    parse(m_str); // throws parse_error
}

url::
url(char const* const ptr)
    : url { std::string(ptr) }
{}

url::
url(char const* const ptr, size_t const len)
    : url { std::string(ptr, len) }
{}

url::
url(std::string_view const view)
    : url { std::string(view) }
{}

url& url::
operator=(std::string&& s)
{
    m_str = std::move(s);
    parse(m_str); // throw parse_error

    return *this;
}

url& url::
operator=(std::string_view const s)
{
    return *this = std::string(s);
}

url& url::
operator=(char const* const ptr)
{
    return *this = std::string(ptr);
}

std::string_view url::
scheme() const
{
    struct handler : event_handler {
        std::string_view result;
        void on_scheme(std::string_view const v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

std::string_view url::
user_info() const
{
    struct handler : event_handler {
        std::string_view result;
        void on_user_info(std::string_view const v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

std::string_view url::
host() const
{
    struct handler : event_handler {
        std::string_view result;
        void on_host(std::string_view const v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

std::string_view url::
port() const
{
    struct handler : event_handler {
        std::string_view result;
        void on_port(std::string_view const v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

std::string_view url::
path() const
{
    struct handler : event_handler {
        std::string_view result;
        void on_path(std::string_view const v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

std::string_view url::
query() const
{
    struct handler : event_handler {
        std::string_view result;
        void on_query(std::string_view const v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

std::string_view url::
fragment() const
{
    struct handler : event_handler {
        std::string_view result;
        void on_fragment(std::string_view const v) override { result = v; }
    } h;

    return parse_component(m_str, h);
}

components url::
components() const
{
    return parse(m_str);
}

std::ostream&
operator<<(std::ostream& oss, url const& u)
{
    oss << u.m_str;
    return oss;
}

} // namespace urls
