#include <url/parser.hpp>
#include <url/error.hpp>

#include <sstream>

#include <boost/algorithm/string.hpp>
#include <boost/config.hpp>
#include <boost/range/iterator_range.hpp>

#define LIKELY(x) BOOST_LIKELY(x)
#define UNLIKELY(x) BOOST_UNLIKELY(x)

// URL       = scheme ":" [hier-part] ["?" query] ["#" fragment]
//
// scheme    = <any OCTET except ":" / "@" / "/" / "?" / "#">
//           ; ":" indicate end of scheme
//           ; "@" indicate error that user_info start without scheme
//           ; "/" indicate error that path start without scheme
//           ; "?" indicate error that path start without scheme
//           ; "#" indicate error that path start without scheme
//
// hier-part = "//" authority ["/" path]
//           / ["/"] path
//
// authority = [user_info "@"] host [":" port]
//
// user_info = *<any OCTET except "@" / "/" / "?" / "#">
//           ; "@" indicate end of user_info
//           ; "/" indicate begining of path
//           ; "?" indicate that parser was looking at path
//           ; "#" indicate that parser was looking at path
//
// host      = *<any OCTET except ":" / "/" / "?" / "#">
//           ; ":" indicate begining of port
//           ; "/" indicate begining of path
//           ; "?" indicate that parser was looking at path
//           ; "#" indicate that parser was looking at path
//
// port      = *DIGIT
//
// path      = *<any OCTET except "?" / "#">
//           ; "?" indicate begining of query
//           ; "#" indicate begining of fragment
//
// query     = *<any OCTET except "#">
//           ; "#" indicate begining of fragment
//
// fragment  = *OCTET

namespace urls {

using iterator = std::string_view::const_iterator;

static auto
make_string_view(iterator const b, iterator const e)
{
    assert(b <= e);
    auto const len = static_cast<size_t>(e - b);
    return std::string_view(b, len);
}

static bool
starts_with(iterator const b, iterator const e, std::string_view const rhs)
{
    namespace ba = boost::algorithm;

    return ba::starts_with(
        boost::make_iterator_range(b, e),
        rhs);
}

static bool
fragment(iterator& it, iterator const e, event_handler& h)
{
    if (LIKELY(it == e || *it != '#')) return true;

    h.on_fragment(make_string_view(++it, e));

    return true;
}

static bool
query(iterator& it, iterator const e, event_handler& h)
{
    if (LIKELY(it == e || *it != '?')) return true;

    auto const start = ++it;
    for (; it != e; ++it) {
        if (UNLIKELY(*it == '#')) break;
    }

    h.on_query(make_string_view(start, it));

    return true;
}

static bool
path(iterator& it, iterator const e, event_handler& h)
{
    auto is_separator = [](auto c) { return c == '?' || c == '#'; };

    auto const start = it;
    for (; it != e; ++it) {
        if (UNLIKELY(is_separator(*it))) break;
    }

    h.on_path(make_string_view(start, it));
    return true;
}

static bool
port(iterator& it, iterator const e, event_handler& h)
{
    auto is_valid = [](auto c) {
        return '0' <= c && c <= '9';
    };

    if (UNLIKELY(it == e)) return true;
    if (LIKELY(*it != ':')) return true;

    auto const start = ++it; // skip ':'
    for (; it != e; ++it) {
        if (UNLIKELY(!is_valid(*it))) break;
    }

    h.on_port(make_string_view(start, it));
    return true;
}

static bool
host(iterator& it, iterator const e, event_handler& h)
{
    auto is_separator = [](auto c) {
        return c == ':' || c == '/' || c == '?' || c == '#';
    };

    auto const start = it;
    for (; it != e; ++it) {
        if (UNLIKELY(is_separator(*it))) break;
    }

    h.on_host(make_string_view(start, it));
    return true;
}

static bool
user_info(iterator& it, iterator const e, event_handler& h)
{
    auto is_valid = [](auto c) {
        return c != '/' && c != '?' && c != '#';
    };

    auto j = it;
    for (; j != e; ++j) {
        if (*j == '/') return true;
        if (*j == '@') break;
    }
    if (LIKELY(j == e)) return true;
    auto end = j;

    auto start = it;
    for (; it != end; ++it) {
        if (UNLIKELY(!is_valid(*it))) {
            h.on_error(parse_errc::invalid_char);
            return false;
        }
    }

    h.on_user_info(make_string_view(start, it));
    ++it; // skip '@'

    return true;
}

static bool
authority(iterator& s, iterator const e, event_handler& h)
{
    if (!UNLIKELY(starts_with(s, e, "//"))) return true;

    s += 2; // skip '//'

    if (UNLIKELY(!user_info(s, e, h))) return false;

    if (UNLIKELY(!host(s, e, h))) return false;

    return port(s, e, h);
}

static bool
hier_part(iterator& s, iterator const e, event_handler& h)
{
    if (UNLIKELY(!authority(s, e, h))) return false;

    return path(s, e, h);
}

static bool
scheme(iterator& it, iterator const e, event_handler& h)
{
    auto constexpr delimiter = ':';

    auto is_valid = [](auto c) {
        return c != '@' && c != '/' && c != '?' && c != '#';
    };

    auto start = it;
    for (; it != e; ++it) {
        auto const ch = *it;

        if (UNLIKELY(ch == delimiter)) {
            h.on_scheme(make_string_view(start, it));
            ++it; // skip delimiter
            return true;
        }
        else if (UNLIKELY(!is_valid(ch))) {
            h.on_error(parse_errc::invalid_char);
            return false;
        }
    }

    h.on_error(parse_errc::no_scheme);

    return false;
}

bool
parse(iterator s, iterator const end, event_handler& h)
{
    if (UNLIKELY(!scheme(s, end, h))) return false;

    if (UNLIKELY(!hier_part(s, end, h))) return false;

    if (UNLIKELY(!query(s, end, h))) return false;

    if (UNLIKELY(!fragment(s, end, h))) return false;

    return true;
}

bool
parse(std::string_view s, event_handler& h)
{
    auto it = s.begin();

    return parse(it, s.end(), h);
}

class handler : public event_handler
{
public:
    handler(components& components) : m_result { components } {}

    void on_scheme(std::string_view const s) override
    {
        m_result.scheme = s;
    }

    void on_user_info(std::string_view const s) override
    {
        m_result.user_info = s;
    }

    void on_host(std::string_view const s) override
    {
        m_result.host = s;
    }

    void on_port(std::string_view const s) override
    {
        m_result.port = s;
    }

    void on_path(std::string_view const s) override
    {
        m_result.path = s;
    }

    void on_query(std::string_view const s) override
    {
        m_result.query = s;
    }

    void on_fragment(std::string_view const s) override
    {
        m_result.fragment = s;
    }

    void on_error(parse_errc const ec) override
    {
        m_error = ec;
    }

    parse_errc error() const { return m_error; }

private:
    components& m_result;
    parse_errc m_error = parse_errc::ok;
};

components
parse(std::string_view const input)
{
    components result;
    handler h { result };

    auto s = input.begin();

    if (UNLIKELY(!parse(s, input.end(), h))) {
        throw parse_error(input, s - input.begin(), h.error());
    }

    return result;
}

//
// parse_error
//
static std::string
make_message(parse_error const& e)
{
    std::ostringstream oss;

    switch (e.code()) {
        case parse_errc::invalid_char:
            oss << "url: invalid character";
            break;
        case parse_errc::no_scheme:
            oss << "url: no scheme";
            break;
        default:
            oss << "url: unknown error";
            break;
    }
    oss << "\n";
    oss << e.url() << "\n";
    for (auto i = 0; i < e.location(); ++i) {
        oss << '-';
    }
    oss << "^";

    return oss.str();
}

parse_error::
parse_error(std::string_view const url,
            int64_t const location,
            parse_errc const code)
    : error { url, location }
    , m_code { code }
{}

char const* parse_error::
what() const noexcept
{
    if (m_message.empty()) {
        m_message = make_message(*this);
    }

    return m_message.c_str();
}

} // namespace urls

