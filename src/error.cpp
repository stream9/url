#include <url/error.hpp>

#include <cstdint>
#include <sstream>
#include <string_view>

namespace urls {

error::
error(std::string_view const url,
      int64_t const location)
    : m_url { url }
    , m_location { location }
{}

} // namespace urls
