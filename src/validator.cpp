#include <url/validator.hpp>

#include <url/components.hpp>
#include <url/error.hpp>

#include "detail/validator.hpp"

#include <cassert>
#include <functional>
#include <sstream>
#include <string_view>

namespace urls::validator {

/*
 * utility
 */
template<typename T>
auto
range(T const& v) -> std::pair<decltype(v.begin()), decltype(v.end())>
{
    return std::make_pair(v.begin(), v.end());
}

size_t
size(iterator const begin, iterator const end)
{
    assert(begin <= end);
    return static_cast<size_t>(end - begin);
}

bool
eoi(iterator const begin, iterator const end)
{
    return begin == end;
}

template<typename Predicate>
bool
token(iterator& it, iterator const end, Predicate pred)
{
    assert(it <= end);

    if (eoi(it, end)) return false;

    if (pred(*it)) {
        ++it;
        return true;
    }
    else {
        return false;
    }
}

bool
token(iterator& it, iterator const end, char const c)
{
    auto pred = [c](auto const ch) { return ch == c; };
    return token(it, end, pred);
}

template<size_t N>
bool
token(iterator& it, iterator const end, char const (&s)[N])
{
    static_assert(N >= 0);
    assert(it <= end);

    if (eoi(it, end)) return false;
    if (size(it, end) < N - 1) return false;

    for (size_t i = 0; i < N-1; ++i) {
        if (it[i] != s[i]) {
            return false;
        }
    }

    it += N - 1;
    return true;
}

template<typename Parser>
bool
zero_or_more(iterator& it, iterator const end,
             Parser parser, error_handler_t err)
{
    if (eoi(it, end)) return true;

    while (it != end) {
        if (!parser(it, end, err)) break;
    }

    return true;
}

template<typename Parser>
bool
one_or_more(iterator& it, iterator const end,
            Parser parser, error_handler_t err)
{
    if (eoi(it, end)) return false;

    if (!parser(it, end, err)) return false;
    while (it != end) {
        if (!parser(it, end, err)) break;
    }

    return true;
}

/*
 * classifier
 */
bool
is_alpha(char const c)
{
    return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z');
}

bool
is_digit(char const c)
{
    return '0' <= c && c <= '9';
}

bool
is_hexdig(char const c)
{
    return is_digit(c) || ('A' <= c && c <= 'F') || ('a' <= c && c <= 'f');
}

/*
 * ALPHA / DIGIT / "-" / "." / "_" / "~"
 */
bool
is_unreserved(char const c)
{
    return is_alpha(c) || is_digit(c) ||
           c == '-' || c == '.' || c == '_' || c == '~';
}

bool
is_sub_delims(char const c)
{
    return c == '!' || c == '$' || c == '&' || c == '\'' || c == '(' ||
           c == ')' || c == '*' || c == '+' || c == ',' || c == ';' ||
           c == '=';
}

/*
 * non-terminal
 */

bool
unreserved(iterator& it, iterator const end, error_handler_t)
{
    return token(it, end, is_unreserved);
}

bool
hexdig(iterator& it, iterator const end, error_handler_t)
{
    return token(it, end, is_hexdig);
}

bool
sub_delims(iterator& it, iterator const end, error_handler_t)
{
    return token(it, end, is_sub_delims);
}

/*
 * "%" HEXDIG HEXDIG (case-insensitive)
 */
bool
pct_encoded(iterator& it, iterator const end, error_handler_t err)
{
    if (size(it, end) < 3) return false;

    if (*it != '%') return false;

    auto save = it;

    if (!is_hexdig(*++it)) {
        if (err(it, validation_errc::invalid_character)) {
            it = save;
            return false;
        }
    }

    if (!is_hexdig(*++it)) {
        if (err(it, validation_errc::invalid_character)) {
            it = save;
            return false;
        }
    }

    ++it;

    return true;
}

/*
 *   DIGIT                ; 0-9
 * / %x31-39 DIGIT        ; 10-99
 * / "1" 2DIGIT           ; 100-199
 * / "2" %x30-34 DIGIT    ; 200-249
 * / "25" %x30-35         ; 250-255
 */
bool
dec_octet(iterator& it, iterator const end, error_handler_t err)
{
    if (eoi(it, end)) return false;

    auto c = *it;
    if (c == '0') {
        ++it;
        return true; // 0
    }
    else if (c == '1') {
        if (eoi(++it, end)) return true; // 1 end
        if (!is_digit(*it)) return true; // 1 !DIGIT

        if (eoi(++it, end)) return true; // 1 DIGIT end
        if (!is_digit(*it)) return true; // 1 DIGIT !DIGIT

        ++it;
        return true; // 1 DIGIT DIGIT
    }
    else if (c == '2') {
        if (eoi(++it, end)) return true; // 2 end

        c = *it;
        if ('0' <= c && c <= '4') {
            if (eoi(++it, end)) return true; // 2 ('0'-'4') end
            if (!is_digit(*it)) return true; // 2 ('0'-'4') !DIGIT

            ++it;
            return true; // 2 ('0'-'4') DIGIT
        }
        else if (c == '5') {
            if (eoi(++it, end)) return true; // 2 5 end
            c = *it;
            if ('0' <= c && c <= '5') {
                ++it;
                return true; // 2 5 ('0'-'5')
            }
            else {
                return true; // 2 5 !DIGIT
            }
        }
        else if ('6' <= c && c <= '9') {
            ++it;
            return true; // 2 6-9
        }
        else {
            return true; // 2 !DIGIT
        }
    }
    else if ('3' <= c && c <= '9') {
        if (eoi(++it, end)) return true; // 3-9 end
        if (!is_digit(*it)) return true; // 3-9 !DIGIT

        ++it;
        return true; // 3-9 DIGIT
    }
    else {
        // !DIGIT
        return !err(it, validation_errc::invalid_character);
    }
}

/*
 * unreserved / pct-encoded / sub-delims / ":" / "@"
 */
bool
pchar(iterator& it, iterator const end, error_handler_t err)
{
    if (eoi(it, end)) return false;

    return unreserved(it, end, err) || pct_encoded(it, end, err) ||
           sub_delims(it, end, err) || token(it, end, ':') ||
           token(it, end, '@');
}

/*
 * *pchar
 */
bool
segment(iterator& it, iterator const end, error_handler_t err)
{
    return zero_or_more(it, end, pchar, err);
}

/*
 * 1*pchar
 */
bool
segment_nz(iterator& it, iterator const end, error_handler_t err)
{
    return one_or_more(it, end, pchar, err);
}

/*
 * dec-octet "." dec-octet "." dec-octet "." dec-octet
 */
bool
ipv4_address(iterator& it, iterator const end, error_handler_t err)
{
    if (eoi(it, end)) return false;

    auto const save = it;

    auto parser = [](auto& it, auto const end, auto err) {
        if (!dec_octet(it, end, err)) return false;
        if (!token(it, end, '.')) return false;
        if (!dec_octet(it, end, err)) return false;
        if (!token(it, end, '.')) return false;
        if (!dec_octet(it, end, err)) return false;
        if (!token(it, end, '.')) return false;
        if (!dec_octet(it, end, err)) return false;

        return true;
    };

    if (!parser(it, end, err)) {
        it = save;
        return false;
    }

    return true;
}

/*
 * 1*4HEXDIG
 */
bool
h16(iterator& it, iterator const end, error_handler_t err)
{
    if (eoi(it, end)) return false;

    if (!hexdig(it, end, err)) return false;

    for (auto i = 0; i < 3; ++i) {
        if (!hexdig(it, end, err)) break;
    }

    return true;
}

/*
 * ( h16 ":" h16 ) / IPv4address
 */
bool
ls32(iterator& it, iterator const end, error_handler_t err)
{
    if (eoi(it, end)) return false;

    auto const save = it;

    if (h16(it, end, err) && token(it, end, ':')
                               && h16(it, end, err))
    {
        return true;
    }

    it = save;
    return ipv4_address(it, end, err);
}

template<size_t N, typename Parser>
bool
repeat(iterator& it, iterator const end,
       Parser parser, error_handler_t err)
{
    auto save = it;

    for (size_t i = 0; i < N; ++i) {
        if (!parser(it, end, err)) {
            it = save;
            return false;
        }
    }

    return true;
}

template<size_t Min, size_t Max, typename Parser>
bool
repeat(iterator& it, iterator const end,
       Parser parser, error_handler_t err)
{
    auto save = it;

    if (Min == 0 && eoi(it, end)) return true;

    size_t n = 0;
    do {
        if (!parser(it, end, err)) break;
        if (++n == Max) break;
    } while (it != end);

    if (n < Min) {
        it = save;
        return false;
    }

    return true;
}

/*
 * IPv6address =                            6( h16 ":" ) ls32
 *             /                       "::" 5( h16 ":" ) ls32
 *             / [               h16 ] "::" 4( h16 ":" ) ls32
 *             / [ *1( h16 ":" ) h16 ] "::" 3( h16 ":" ) ls32
 *             / [ *2( h16 ":" ) h16 ] "::" 2( h16 ":" ) ls32
 *             / [ *3( h16 ":" ) h16 ] "::"    h16 ":"   ls32
 *             / [ *4( h16 ":" ) h16 ] "::"              ls32
 *             / [ *5( h16 ":" ) h16 ] "::"              h16
 *             / [ *6( h16 ":" ) h16 ] "::"
 */
bool
ipv6_address(iterator& it, iterator const end, error_handler_t err)
{
    if (eoi(it, end)) return false;

    auto save = it;

    auto h16_colon = [](auto& it, auto const end, auto err) {
        auto save = it;

        if (h16(it, end, err) && token(it, end, ':')) {
            return true;
        }

        it = save;
        return false;;
    };

    auto empty_group = [&]() {
        return token(it, end, "::");
    };

    auto n_h16_colon_then_h16 = [&](int const n) {
        auto save = it;

        for (auto i = 0; i < n; ++i) {
            if (!h16_colon(it, end, err)) break;
            if (*it == ':') { // double colon -> empty group
                --it;
                return true;
            }
        }

        if (!h16(it, end, err)) {
            it = save;
            return false;
        }

        return true;
    };

    // 6( h16 ":" ) ls32
    if (repeat<6>(it, end, h16_colon, err) && ls32(it, end, err)) {
        return true;
    }

    // "::" 5( h16 ":" ) ls32
    it = save;
    if (empty_group() && repeat<5>(it, end, h16_colon, err) && ls32(it, end, err)) {
        return true;
    }

    // [ h16 ] "::" 4( h16 ":" ) ls32
    it = save;
    h16(it, end, err);
    if (empty_group() && repeat<4>(it, end, h16_colon, err) && ls32(it, end, err)) {
        return true;
    }

    // [ *1( h16 ":" ) h16 ] "::" 3( h16 ":" ) ls32
    it = save;
    n_h16_colon_then_h16(1);
    if (empty_group() && repeat<3>(it, end, h16_colon, err) && ls32(it, end, err)) {
        return true;
    }

    // [ *2( h16 ":" ) h16 ] "::" 2( h16 ":" ) ls32
    it = save;
    n_h16_colon_then_h16(2);
    if (empty_group() && repeat<2>(it, end, h16_colon, err)
                                      && ls32(it, end, err))
    {
        return true;
    }

    // [ *3( h16 ":" ) h16 ] "::" h16 ":" ls32
    it = save;
    n_h16_colon_then_h16(3);
    if (empty_group() && h16_colon(it, end, err) && ls32(it, end, err)) {
        return true;
    }

    // [ *4( h16 ":" ) h16 ] "::" ls32
    it = save;
    n_h16_colon_then_h16(4);
    if (empty_group() && ls32(it, end, err)) {
        return true;
    }

    // [ *5( h16 ":" ) h16 ] "::" h16
    it = save;
    n_h16_colon_then_h16(5);
    if (empty_group() && h16(it, end, err)) {
        return true;
    }

    // [ *6( h16 ":" ) h16 ] "::"
    it = save;
    n_h16_colon_then_h16(6);
    if (empty_group()) {
        return true;
    }

    err(it, validation_errc::invalid_ipv6_address);
    it = save;
    return false;
}

/*
 * "v" 1*HEXDIG "." 1*( unreserved / sub-delims / ":" )
 */
bool
ipvfuture(iterator& it, iterator const end, error_handler_t err)
{
    if (eoi(it, end)) return false;

    auto save = it;

    if (!token(it, end, 'v')) {
        it = save;
        return false;
    }

    if (!one_or_more(it, end, hexdig, err)) {
        it = save;
        return false;
    }

    if (!token(it, end, '.')) {
        it = save;
        return false;
    }

    auto is_valid = [](auto& it, auto const end, auto err) {
        return unreserved(it, end, err) || sub_delims(it, end, err) ||
               token(it, end, ':');
    };
    if (!one_or_more(it, end, is_valid, err)) {
        it = save;
        return false;
    }

    return true;
}

/*
 * IP-literal = "[" (IPv6address / IPvFuture) "]"
 */
bool
ip_literal(iterator& it, iterator const end, error_handler_t err)
{
    if (eoi(it, end)) return false;
    if (*it != '[') return false;

    auto const save = it++;

    if (ipv6_address(it, end, err) || ipvfuture(it, end, err)) {
        if (*it == ']') {
            ++it;
            return true;
        }
    }

    err(it, validation_errc::invalid_ip_literal);
    it = save;
    return false;
}

/*
 * *( unreserved / pct-encoded / sub-delims )
 */
bool
reg_name(iterator& it, iterator const end, error_handler_t err)
{
    auto parser = [&](auto& it, auto const end, auto err) {
        return unreserved(it, end, err) || sub_delims(it, end, err)
                                        || pct_encoded(it, end, err);
    };

    if (!zero_or_more(it, end, parser, err)) return false;

    return true;
}

/*
 * url component
 */

/*
 * ALPHA *(ALPHA / DIGIT / "+" / "-" / ".")
 */
bool
scheme(std::string_view const s, error_handler_t err)
{
    if (s.empty()) return true; // ok on reletive URL

    auto [it, end] = range(s);

    if (!is_alpha(*it)) {
        if (err(it, validation_errc::invalid_character)) {
            return false;
        }
    }
    ++it;

    auto is_valid = [](auto const c) {
        return is_alpha(c) || is_digit(c) || c == '+' || c == '-' || c == '.';
    };

    for (; it != end; ++it) {
        if (!is_valid(*it)) {
            if (err(it, validation_errc::invalid_character)) {
                return false;
            }
        }
    }

    return true;
}

/*
 * *(unreserved / pct-encoded / sub-delims / ":")
 */
bool
user_info(std::string_view const s, error_handler_t err)
{
    auto is_valid = [&](auto& it, auto const end, auto) {
        return unreserved(it, end, err) || pct_encoded(it, end, err) ||
               sub_delims(it, end, err) || token(it, end, ':');
    };

    auto [it, end] = range(s);

    while (true) {
        zero_or_more(it, end, is_valid, err);

        if (eoi(it, end)) {
            break;
        }
        else {
            if (err(it, validation_errc::invalid_character)) {
                return false;
            }
        }
    }

    return true;
}

/*
 * IP-literal / IPv4address / reg-name
 */
bool
host(std::string_view const s, error_handler_t err)
{
    if (s.empty()) return true;

    auto [it, e] = range(s);

    auto rule = [&] {
        if (ip_literal(it, e, err)) return true;
        if (ipv4_address(it, e, err)) return true;
        if (reg_name(it, e, err)) return true;

        return false;
    };

    if (!rule()) return false;
    if (!eoi(it, e)) {
        if (err(it, validation_errc::invalid_character)) {
            return false;
        }
    }

    return true;
}

/*
 * *DIGIT
 */
bool
port(std::string_view const s, error_handler_t err)
{
    for (auto [it, end] = range(s); !eoi(it, end); ++it) {
        if (!is_digit(*it)) {
            if (err(it, validation_errc::invalid_character)) {
                return false;
            }
        }
    }

    return true;
}

/*
 * *( "/" segment)
 */
bool
path_abempty(std::string_view const s, error_handler_t err)
{
    if (s.empty()) return true;

    auto [it, end] = range(s);

    while (it != end) {
        if (!token(it, end, '/')) return false;
        if (!segment(it, end, err)) return false;
    }

    return true;
}

/*
 * "/" [ segment-nz *( "/" segment ) ]
 */
bool
path_absolute(std::string_view const s, error_handler_t err)
{
    if (s.empty()) return false;

    auto [it, end] = range(s);

    if (!token(it, end, '/')) return false;

    if (eoi(it, end)) return true;

    if (!segment_nz(it, end, err)) return false;

    auto segments = [](auto& it, auto const end, auto err) {
        if (!token(it, end, '/')) return false;
        if (!segment(it, end, err)) return false;
        return true;
    };

    if (!zero_or_more(it, end, segments, err)) return false;

    return eoi(it, end);
}

/*
 * segment_nz *( "/" segment )
 */
bool
path_rootless(std::string_view const s, error_handler_t err)
{
    if (s.empty()) return false;

    auto [it, end] = range(s);

    if (!segment_nz(it, end, err)) return false;

    auto segments = [](auto& it, auto const end, auto err) {
        if (!token(it, end, '/')) return false;
        if (!segment(it, end, err)) return false;
        return true;
    };

    if (!zero_or_more(it, end, segments, err)) return false;

    return eoi(it, end);
}

/*
 * 0<pchar>
 */
bool
path_empty(std::string_view const s, error_handler_t)
{
    return s.empty();
}

/*
 * *( pchar / "/" / "?" )
 */
bool
query(std::string_view const s, error_handler_t err)
{
    if (s.empty()) return true;

    auto [it, end] = range(s);

    auto is_valid = [](auto& it, auto const end, auto err) {
        return pchar(it, end, err)
            || token(it, end, '/') || token(it, end, '?');
    };

    if (!zero_or_more(it, end, is_valid, err)) return false;

    return eoi(it, end);
}

/*
 * *( pchar / "/" / "?" )
 */
bool
fragment(std::string_view const s, error_handler_t err)
{
    if (s.empty()) return true;

    auto [it, end] = range(s);

    auto is_valid = [](auto& it, auto const end, auto err) {
        return pchar(it, end, err)
            || token(it, end, '/') || token(it, end, '?');
    };

    if (!zero_or_more(it, end, is_valid, err)) return false;

    return eoi(it, end);
}

bool
validate(components const& components)
{
    auto has_authority = [&]() {
        return !components.host.empty();
    };

    auto err = [&](auto& it, auto code) -> bool {
        auto const url = serialize(components);

        throw validation_error {
            url,
            it - url.begin(),
            code
        };
    };

    if (!scheme(components.scheme, err)) return false;
    if (!user_info(components.user_info, err)) return false;
    if (!host(components.host, err)) return false;
    if (!port(components.port, err)) return false;

    auto const& path = components.path;
    if (has_authority()) {
        if (!path_abempty(path, err)) return false;
    }
    else {
        if (!path_absolute(path, err) && !path_rootless(path, err) &&
                                         !path_empty(path, err))
        {
            return false;
        }
    }

    if (!query(components.query, err)) return false;
    if (!fragment(components.fragment, err)) return false;

    return true;
}

} // namespace urls;;validator

namespace urls {

static std::string
make_message(validation_error const& e)
{
    std::ostringstream oss;

    switch (e.code()) {
        case validation_errc::empty_scheme:
            oss << "url: scheme can't be empty";
            break;
        case validation_errc::invalid_character:
            oss << "url: invalid character";
            break;
        case validation_errc::premature_end_of_input:
            oss << "url: input end prematurely";
            break;
        case validation_errc::invalid_ip_literal:
            oss << "url: invalid IP literal";
            break;
        case validation_errc::invalid_ipv6_address:
            oss << "url: invalid IPv6 address";
            break;
        default:
            oss << "url: unknown error";
            break;
    }
    oss << "\n";
    oss << e.url() << "\n";
    for (auto i = 0; i < e.location(); ++i) {
        oss << '-';
    }
    oss << "^";

    return oss.str();
}

validation_error::
validation_error(std::string_view const url,
                 int64_t const location,
                 validation_errc const code)
    : error { url, location }
    , m_code { code }
{}

char const* validation_error::
what() const noexcept
{
    if (m_message.empty()) {
        m_message = make_message(*this);
    }

    return m_message.c_str();
}

} // namespace urls
