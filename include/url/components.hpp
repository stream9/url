#ifndef URL_URL_COMPONENTS_HPP
#define URL_URL_COMPONENTS_HPP

#include <string_view>

namespace urls {

struct components
{
    std::string_view scheme;
    std::string_view user_info;
    std::string_view host;
    std::string_view port;
    std::string_view path;
    std::string_view query;
    std::string_view fragment;
};

std::string_view serialize(components const& c);

} // namespace urls

#endif // URL_URL_COMPONENTS_HPP
