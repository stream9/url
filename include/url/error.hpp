#ifndef URL_ERROR_HPP
#define URL_ERROR_HPP

#include <cstdint>
#include <exception>
#include <string>
#include <string_view>

namespace urls {

// exception

class error : public std::exception
{
public:
    error(std::string_view url, int64_t location);

    // accessor
    std::string_view url() const { return m_url; }
    int64_t location() const { return m_location; }

private:
    std::string m_url;
    int64_t m_location;

};

} // namespace urls

#endif // URL_ERROR_HPP
