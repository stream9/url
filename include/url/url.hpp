#ifndef URL_URL_HPP
#define URL_URL_HPP

#include "components.hpp"

#include <iosfwd>
#include <string>
#include <string_view>

namespace urls {

class url
{
public:
    using size_type = std::string::size_type;
    using const_iterator = std::string::const_iterator;
    using iterator = const_iterator;

public:
    url(std::string&&);
    url(std::string_view);
    url(char const*);
    url(char const* ptr, size_t len);

    template<typename It>
    url(It begin, It end)
        : url { std::string(begin, end) }
    {}

    url& operator=(std::string&&);
    url& operator=(std::string_view);
    url& operator=(char const*);

    // query
    std::string_view scheme() const;
    std::string_view user_info() const;
    std::string_view host() const;
    std::string_view port() const;
    std::string_view path() const;
    std::string_view query() const;
    std::string_view fragment() const;

    urls::components components() const;

    const_iterator begin() const { return m_str.begin(); }
    const_iterator end() const { return m_str.end(); }

    size_type size() const { return m_str.size(); }

    // operator
    bool operator==(url const& rhs) const { return m_str == rhs.m_str; }
    bool operator!=(url const& rhs) const { return m_str != rhs.m_str; }
    bool operator<(url const& rhs) const { return m_str < rhs.m_str; }
    bool operator<=(url const& rhs) const { return m_str <= rhs.m_str; }
    bool operator>(url const& rhs) const { return m_str > rhs.m_str; }
    bool operator>=(url const& rhs) const { return m_str >= rhs.m_str; }

    bool operator==(std::string const& rhs) const { return m_str == rhs; }
    bool operator!=(std::string const& rhs) const { return m_str != rhs; }
    bool operator<(std::string const& rhs) const { return m_str < rhs; }
    bool operator<=(std::string const& rhs) const { return m_str <= rhs; }
    bool operator>(std::string const& rhs) const { return m_str > rhs; }
    bool operator>=(std::string const& rhs) const { return m_str >= rhs; }

    bool operator==(std::string_view rhs) const { return m_str == rhs; }
    bool operator!=(std::string_view rhs) const { return m_str != rhs; }
    bool operator<(std::string_view rhs) const { return m_str < rhs; }
    bool operator<=(std::string_view rhs) const { return m_str <= rhs; }
    bool operator>(std::string_view rhs) const { return m_str > rhs; }
    bool operator>=(std::string_view rhs) const { return m_str >= rhs; }

    bool operator==(char const* rhs) const { return m_str == rhs; }
    bool operator!=(char const* rhs) const { return m_str != rhs; }
    bool operator<(char const* rhs) const { return m_str < rhs; }
    bool operator<=(char const* rhs) const { return m_str <= rhs; }
    bool operator>(char const* rhs) const { return m_str > rhs; }
    bool operator>=(char const* rhs) const { return m_str >= rhs; }

    operator std::string_view() const { return m_str; }

    friend std::ostream& operator<<(std::ostream& oss, url const& u);

private:
    std::string m_str;
};

// std::string
inline bool
operator==(std::string const& lhs, url const& rhs)
{
    return rhs == lhs;
}

inline bool
operator!=(std::string const& lhs, url const& rhs)
{
    return rhs != lhs;
}

inline bool
operator<(std::string const& lhs, url const& rhs)
{
    return rhs >= lhs;
}

inline bool
operator<=(std::string const& lhs, url const& rhs)
{
    return rhs > lhs;
}

inline bool
operator>(std::string const& lhs, url const& rhs)
{
    return rhs <= lhs;
}

inline bool
operator>=(std::string const& lhs, url const& rhs)
{
    return rhs < lhs;
}

// std::string_view
inline bool
operator==(std::string_view const lhs, url const& rhs)
{
    return rhs == lhs;
}

inline bool
operator!=(std::string_view const lhs, url const& rhs)
{
    return rhs != lhs;
}

inline bool
operator<(std::string_view const lhs, url const& rhs)
{
    return rhs >= lhs;
}

inline bool
operator<=(std::string_view const lhs, url const& rhs)
{
    return rhs > lhs;
}

inline bool
operator>(std::string_view const lhs, url const& rhs)
{
    return rhs <= lhs;
}

inline bool
operator>=(std::string_view const lhs, url const& rhs)
{
    return rhs < lhs;
}

// char pointer
inline bool
operator==(char const* const lhs, url const& rhs)
{
    return rhs == lhs;
}

inline bool
operator!=(char const* const lhs, url const& rhs)
{
    return rhs != lhs;
}

inline bool
operator<(char const* const lhs, url const& rhs)
{
    return rhs >= lhs;
}

inline bool
operator<=(char const* const lhs, url const& rhs)
{
    return rhs > lhs;
}

inline bool
operator>(char const* const lhs, url const& rhs)
{
    return rhs <= lhs;
}

inline bool
operator>=(char const* const lhs, url const& rhs)
{
    return rhs < lhs;
}

} // namespace urls

#endif // URL_URL_HPP
