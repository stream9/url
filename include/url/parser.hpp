#ifndef URL_PARSER_HPP
#define URL_PARSER_HPP

#include "error.hpp"
#include "components.hpp"

#include <string_view>

namespace urls {

class event_handler;

// functions

components parse(std::string_view url);

bool parse(std::string_view url, event_handler&);
bool parse(char const* begin, char const* const end, event_handler&);

// types

enum class parse_errc {
    ok = 0,
    invalid_char,
    no_scheme,
};

class event_handler
{
public:
    virtual ~event_handler() {};

    virtual void on_scheme(std::string_view) {}
    virtual void on_user_info(std::string_view) {}
    virtual void on_host(std::string_view) {}
    virtual void on_port(std::string_view) {}
    virtual void on_path(std::string_view) {}
    virtual void on_query(std::string_view) {}
    virtual void on_fragment(std::string_view) {}

    virtual void on_error(parse_errc) {}
};

class parse_error : public error
{
public:
    parse_error(std::string_view url, int64_t location, parse_errc);

    // accessor
    parse_errc code() const { return m_code; }

    // override std::exception
    char const* what() const noexcept override;

private:
    parse_errc m_code;
    mutable std::string m_message;
};

} // namespace urls

#endif // URL_PARSER_HPP
