#ifndef URL_VALIDATOR_HPP
#define URL_VALIDATOR_HPP

#include "components.hpp"
#include "error.hpp"

namespace urls {

bool validate(components const&);

enum class validation_errc {
    ok = 0,
    empty_scheme,
    invalid_character,
    premature_end_of_input,
    invalid_ip_literal,
    invalid_ipv6_address,
};

class validation_error : public error
{
public:
    validation_error(std::string_view url,
                     int64_t location,
                     validation_errc code);

    // accessor
    validation_errc code() const { return m_code; }

    // override std::exception
    char const* what() const noexcept override;

private:
    validation_errc m_code;
    mutable std::string m_message;
};


} // namespace urls

#endif // URL_VALIDATOR_HPP
