#ifndef URI_TEST_UTILITY_HPP
#define URI_TEST_UTILITY_HPP

#include <algorithm>
#include <string>
#include <string_view>
#include <utility>

#include <boost/test/unit_test.hpp>

namespace testing {

template<typename T>
auto
begin_end(T const& v) -> std::pair<decltype(v.begin()), decltype(v.end())>
{
    return std::make_pair(v.begin(), v.end());
}

inline std::string
invert(std::string_view const input)
{
    std::string result;
    auto [begin, end] = begin_end(input);

    for (auto i = 0; i <= 255; ++i) {
        auto const c = static_cast<char>(i);
        if (std::find(begin, end, c) == end) {
            result.push_back(c);
        }
    }

    BOOST_TEST(input.size() + result.size() == 256);
    return result;
}

} // namespace testing

#endif // URI_TEST_UTILITY_HPP
