#include <url/url.hpp>

#include <cstring>
#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace urls;

BOOST_AUTO_TEST_SUITE(url_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(copy_)
    {
        url const u1 { "http://example.com" };
        url const u2 { u1 };

        BOOST_TEST(u1 == u2);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        url u1 { "http://example.com" };
        url const u2 { std::move(u1) };

        BOOST_TEST(u2 == "http://example.com");
    }

    BOOST_AUTO_TEST_CASE(string_copy_)
    {
        std::string const s = "http://www.sample.com";

        url const u { s };

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(string_move_)
    {
        std::string const s = "http://www.sample.com";

        url const u { std::string(s) };

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(string_view_)
    {
        std::string_view const s = "http://www.sample.com";

        url const u { s };

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        auto const s = "http://www.sample.com";

        url const u { s };

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(pointer_len_)
    {
        auto const s = "http://www.sample.com";

        url const u { s, std::strlen(s) };

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(range_)
    {
        std::string_view const s = "http://www.sample.com";

        url const u { s.begin(), s.end() };

        BOOST_TEST(u == s);
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(assignment_)

    BOOST_AUTO_TEST_CASE(copy_)
    {
        url const u1 { "http://example.com" };
        url u2 { "http://sample.com" };

        u2 = u1;

        BOOST_TEST(u2 == u1);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        url u1 { "http://example.com" };
        url u2 { "http://sample.com" };

        u2 = std::move(u1);

        BOOST_TEST(u2 == "http://example.com");
    }

    BOOST_AUTO_TEST_CASE(string_copy_)
    {
        std::string const s = "http://www.sample.com";
        url u { "http://example.com" };

        u = s;

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(string_move_)
    {
        std::string s = "http://www.sample.com";
        url u { "http://example.com" };

        u = std::move(s);

        BOOST_TEST(u == "http://www.sample.com");
    }

    BOOST_AUTO_TEST_CASE(string_view_)
    {
        std::string_view const s = "http://www.sample.com";
        url u { "http://example.com" };

        u = s;

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        auto const s = "http://www.sample.com";
        url u { "http://example.com" };

        u = s;

        BOOST_TEST(u == s);
    }

BOOST_AUTO_TEST_SUITE_END() // assignment_

BOOST_AUTO_TEST_SUITE(conversion_)

    BOOST_AUTO_TEST_CASE(to_string_)
    {
        url const u { "http://example.com" };

        std::string const s { u };

        BOOST_TEST(s == u);
    }

    BOOST_AUTO_TEST_CASE(to_string_view_)
    {
        url const u { "http://example.com" };

        std::string_view const s = u;

        BOOST_TEST(s == u);
    }

BOOST_AUTO_TEST_SUITE_END() // conversion_

BOOST_AUTO_TEST_SUITE_END() // url_

} // namespace testing
